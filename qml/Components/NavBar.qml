import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

ToolBar {
    property string url
    property bool landscape: false

    property QtObject colors: QtObject {
        readonly property color divider: '#FFDD00'
        readonly property color highlight: '#FFDD00'
        readonly property color text: '#F9F9FA'
        readonly property color background: '#1B1A26'
    }

    GridLayout {
        anchors {
            fill: parent
            leftMargin: landscape ? 0 : units.gu(1)
            rightMargin: landscape ? 0 : units.gu(1)
            topMargin: landscape ? units.gu(6) : 0
            bottomMargin: landscape ? units.gu(6) : 0
        }

        columns: landscape ? 1 : 11
        rows: landscape ? 11 : 1

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)
                
                contentItem: Ubuntu.Icon {
                    name: 'go-home'
                    color: (url == 'https://vrv.co/') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://vrv.co/'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            
            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Ubuntu.Icon {
                    name: 'media-playlist'
                    color: (url == 'https://vrv.co/watchlist') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://vrv.co/watchlist'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            
            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Ubuntu.Icon {
                    name: 'view-grid-symbolic'
                    color: (url == 'https://vrv.co/#channels') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://vrv.co/#channels'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            
            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Ubuntu.Icon {
                    name: 'toolkit_input-search'
                    color: (url == 'https://vrv.co/#search') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://vrv.co/#search'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            
            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Ubuntu.Icon {
                    name: 'settings'
                    color: (url == 'https://vrv.co/account') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://vrv.co/account'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }
    }

    background: Rectangle {
        color: colors.background

        Rectangle {
            visible: !landscape
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }

            height: units.dp(2)
            color: colors.divider
        }
        
        Rectangle {
            visible: landscape
            anchors {
                bottom: parent.bottom
                right: parent.right
                top: parent.top
            }

            width: units.dp(2)
            color: colors.divider
        }
    }
}
